package domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "A")
public class Administrativo extends Funcionario {

	@Column(name = "turno", columnDefinition = "character varying (20)", nullable = false)
	private String turno;
}
