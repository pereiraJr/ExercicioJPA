package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "estado", schema = "public")
public class Estado {
	@Id
	@Column(name = "sigla", columnDefinition = "char(2)", nullable = false, unique = true)
	String sigla;

	@Column(name = "nome", columnDefinition = "character varying (15)", nullable = false)
	String nome;

	@OneToMany(mappedBy = "estado", targetEntity = Cidade.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	List<Cidade> cidades;
 
	String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
}
