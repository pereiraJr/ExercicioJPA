package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "funcionario", schema = "public", uniqueConstraints = {
		@UniqueConstraint(name = "uk", columnNames = { "rg", "rg_orgao_expedidor", "rg_uf" }) }, indexes = {
				@Index(name = "rg", unique = true, columnList = "rg, rg_orgao_expedidor, rg_uf") })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo", length = 1, discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("F")
public class Funcionario implements Serializable {

	@Column(name = "cpf", columnDefinition = "character varying (50)", nullable = false, unique = true)
	private String cpf;

	@Column(name = "nome", columnDefinition = "character varying (50)", nullable = false)
	private String nome;
	@Id
	@Column(name = "rg", columnDefinition = "character varying (20)", nullable = false)
	private String rg;

	@Column(name = "rg_orgao_expedidor", columnDefinition = "character varying (20)", nullable = false)
	private String rgOrgaoExpedidor;

	@Column(name = "rg_uf", columnDefinition = "character varying (15)", nullable = false)
	private String rgUf;

	@Column(name = "telefone", nullable = true)
	private Integer telefone;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento")
	private Date dataNascimento;

	@Embedded
	private Endereco endereco;

	/*
	 * TODO Aqui vai uma observa��o muito importante, repare no atributo tipo da
	 * classe Pessoa, ele deve possuir o valor dos atributos insertable e
	 * updatable definidos como false, caso contr�rio o Hibernate n�o conseguir�
	 * realizar a cria��o das tabelas no banco de dados.
	 */
	@Column(name = "tipo", nullable = false, insertable = false, updatable = false)
	private String tipo;
}
