package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pessoa_juridica", schema = "public")
public class PessoaJuridica {

	@Id
	private String cnpj;
	@Column(name = "nome", columnDefinition = "character varying (15)", nullable = false)
	private String nome;

	@ManyToOne
	@JoinColumn(name = "ramo_atividade_id", nullable = false)
	private RamoAtividade ramoAtividade;

	@Column(name = "faturamento")
	private Long faturamento;

	
	/*
	 * TODO
	 * 
	 * @JoinTable(name = "vendedor_pessoaJuridica", joinColumns = {
	 * 
	 * @JoinColumn(name = "vendedores_rg") }, inverseJoinColumns =
	 * { @JoinColumn(name = "cliente_cnpj") })
	 * 
	 * 
	 */
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "vendedor_pessoaJuridica", joinColumns = { @JoinColumn() })
	private List<Vendedor> vendedores;

}