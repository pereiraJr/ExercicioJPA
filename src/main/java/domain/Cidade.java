package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cidade", schema = "public")
public class Cidade {
	@Id
	@Column(name = "sigla", columnDefinition = "char(2)", nullable = false)
	private char sigla;

	@Column(name = "nome", columnDefinition = "character varying (15)", nullable = false)
	private String nome;

	@ManyToOne
	@JoinColumn(name = "sigla", nullable = false, columnDefinition = "char(2)", insertable = false, updatable = false)
	private Estado estado;

}
