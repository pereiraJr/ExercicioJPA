package domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class Endereco {
	@Column(name = "logradouro", columnDefinition = "character varying (15)", nullable = false)
	private String logradouro;

	@Column(name = "bairro", columnDefinition = "character varying (15)", nullable = false)
	private String bairro;

	@ManyToOne(optional = false)
	private Cidade cidade;

}
