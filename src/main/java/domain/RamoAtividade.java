package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ramo_atividade", schema = "public")
public class RamoAtividade {

	@Id
	@Column(name = "id", columnDefinition = "serial", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "nome", columnDefinition = "character varying(15)", nullable = false)
	private String nome;

	@OneToMany(mappedBy = "ramoAtividade", targetEntity = PessoaJuridica.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PessoaJuridica> pessoasJuridica;

}
