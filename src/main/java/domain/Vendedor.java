package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import enums.SituacaoFuncionarioEnum;

@Entity
@DiscriminatorValue(value = "V")
public class Vendedor extends Funcionario {

	private double percentualComissao;

	private SituacaoFuncionarioEnum situacao;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "vendedores")
	private List<PessoaJuridica> clientes;

}
