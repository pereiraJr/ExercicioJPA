package enums;

public enum SituacaoFuncionarioEnum {

	ATIVO("A"), SUSPENSO("S");

	private String cod;

	private SituacaoFuncionarioEnum(String cod) {
		this.cod = cod;
	}

	public String getCod() {
		return cod;
	}
}
