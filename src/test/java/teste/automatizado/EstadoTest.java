package teste.automatizado;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Test;

import domain.Estado;
import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class EstadoTest {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicio02-teste");

	EntityManager em = emf.createEntityManager();

	@Test
	public void inserirEstado() {

		em.getTransaction().begin();

		Estado estadoInserido = new Estado();
		estadoInserido.setSigla("BA");
		estadoInserido.setNome("Bahia");

		em.persist(estadoInserido);
		em.getTransaction().commit();

		Estado estadoRetorno = em.find(Estado.class, "BA");

		assertEquals(estadoInserido, estadoRetorno);

		em.close();
		emf.close();
	}

}
